package robots.world;

public interface CommandInaccuracy {
    RobotCommand affect(World world, RobotCommand command);
}
