package robots.world;

import robots.geometry.Vector;

public class Wind implements EnvironmentNoise {
	public final Vector velocity;

	public Wind(Vector velocity) {
		this.velocity = velocity;
	}

    public World affect(World world) {
        Vector newPosition = world.robot.position.add(velocity.multiply(World.TIME_INTERVAL));
        Robot newRobot = new Robot(world.robot.config, newPosition, world.robot.direction);

        return new World(newRobot, world.robotAI, world.target, world.environmentNoises, world.commandInaccuracies);
    }

    public String toString() {
        return String.format("Wind(%s)", velocity);
    }
}
