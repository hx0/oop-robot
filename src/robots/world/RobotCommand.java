package robots.world;

public class RobotCommand {
	public final double velocity, angleVelocity;

	public RobotCommand(double velocity, double angleVelocity) {
		this.velocity = velocity;
		this.angleVelocity = angleVelocity;
	}

	public String toString() {
		return String.format("velocity = %.3f, angleVelocity = %.3f",
				velocity, angleVelocity);
	}
}
