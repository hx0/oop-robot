package robots.world;

public class Simulator {
    private String description;
    private World world;
    private double currentTime;

	public Simulator(String description, World world) {
        this.description = description;
		this.world = world;
        currentTime = 0;
	}

    public String getDescription() {
        return description;
    }

    public World getWorld() {
        return world;
    }

    public double getCurrentTime() {
        return currentTime;
    }

    public final static double TARGET_REACHING_EPS = 1e-2;

	public boolean makeStep() {
        if (world.robot.getHeadPosition().subtract(world.target).length() <  TARGET_REACHING_EPS)
            return true;

        RobotCommand command = world.robotAI.nextMove(world.robot, world);
        if (command == null)
            throw new RuntimeException("The algorithm failed to reach the target");

        currentTime += World.TIME_INTERVAL;
        for (CommandInaccuracy inaccuracy : world.commandInaccuracies)
        	command = inaccuracy.affect(world, command);
        for (EnvironmentNoise noise : world.environmentNoises)
        	world = noise.affect(world);

        Robot newRobot = world.robot.move(command);
        world = new World(newRobot, world.robotAI, world.target,
                world.environmentNoises, world.commandInaccuracies);
        return false;
	}
}
