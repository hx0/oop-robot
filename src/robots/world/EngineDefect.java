package robots.world;

import java.util.Random;

public class EngineDefect implements CommandInaccuracy {
    public final double velocityDefect, angleVelocityDefect;

    public EngineDefect(double velocityDefect, double angleVelocityDefect) {
        this.velocityDefect = velocityDefect;
        this.angleVelocityDefect = angleVelocityDefect;
    }

    private static double affectTo(double value, Random random, double defect) {
        return value - defect + (2 * defect) * random.nextDouble();
    }

    public RobotCommand affect(World world, RobotCommand command) {
        Random random = new Random();
        return new RobotCommand(
                affectTo(command.velocity, random, velocityDefect),
                affectTo(command.angleVelocity, random, angleVelocityDefect));
    }

    public String toString() {
        return String.format("EngineDefect(%.4f, %.4f)", velocityDefect, angleVelocityDefect);
    }
}
