package robots.world;

import robots.geometry.*;

public class Robot {
	public final RobotConfig config;
	public final Vector position;
	public final Angle direction;

	public Robot(RobotConfig config, Vector position, Angle direction) {
		this.config = config;
		this.position = position;
		this.direction = direction;
	}

	public Vector getHeadOffset() {
		return new Vector(config.radius, 0).rotate(direction);
	}

	public Vector getHeadPosition() {
		return position.add(getHeadOffset());
	}

	private static double limit(double value, double maxAbs) {
		if (value < -maxAbs)
			return -maxAbs;
		if (value > maxAbs)
			return maxAbs;
		return value;
	}

	private static final double EPS = 1e-6;

	public Robot move(RobotCommand command) {
		double timeInterval = World.TIME_INTERVAL;
		command = new RobotCommand(
				limit(command.velocity, config.maxVelocity),
				limit(command.angleVelocity, config.maxAngleVelocity));

		if (Math.abs(command.angleVelocity) < EPS) {
			double distance = command.velocity * timeInterval;
			Vector newPosition = position.add(new Vector(distance, 0).rotate(direction));
			return new Robot(config, newPosition, direction);
		}

		Vector newPosition = position.add(new Vector(
				command.velocity / command.angleVelocity * (
						Math.sin(direction.value + command.angleVelocity * timeInterval) -
								Math.sin(direction.value)
				),
				command.velocity / command.angleVelocity * (
						Math.cos(direction.value) -
								Math.cos(direction.value + command.angleVelocity * timeInterval)
				)));
		Angle newDirection = new Angle(
				direction.value + command.angleVelocity * timeInterval);
		return new Robot(config, newPosition, newDirection);
	}

	public String toString() {
		return String.format("config = {%s}, position = %s, direction = %s",
				config, position, direction);
	}
}
