package robots.world;

public interface RobotAI {
    RobotCommand nextMove(Robot robot, World world);
}
