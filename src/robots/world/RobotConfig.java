package robots.world;

public class RobotConfig {
	public final double radius, maxVelocity, maxAngleVelocity;

	public RobotConfig(double radius, double maxVelocity, double maxAngleVelocity) {
		this.radius = radius;
		this.maxVelocity = maxVelocity;
		this.maxAngleVelocity = maxAngleVelocity;
	}

	public String toString() {
		return String.format("radius = %.3f, maxVelocity = %.3f, maxAngleVelocity = %.3f",
				radius, maxVelocity, maxAngleVelocity);
	}
}
