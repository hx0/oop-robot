package robots.world;

public interface EnvironmentNoise {
	World affect(World world);
}
