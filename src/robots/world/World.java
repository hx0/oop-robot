package robots.world;

import robots.geometry.*;

import java.util.List;

public class World {
    public final Robot robot;
    public final RobotAI robotAI;
    public final Vector target;
    public final List<EnvironmentNoise> environmentNoises;
    public final List<CommandInaccuracy> commandInaccuracies;

    public final static double TIME_INTERVAL = 1e-2;

    public World(Robot robot, RobotAI robotAI, Vector target,
                 List<EnvironmentNoise> environmentNoises, List<CommandInaccuracy> commandInaccuracies) {
        this.robot = robot;
        this.robotAI = robotAI;
        this.target = target;
        this.environmentNoises = environmentNoises;
        this.commandInaccuracies = commandInaccuracies;
    }

    public String toString() {
        return String.format("robot = {%s}, target = %s", robot, target);
    }
}
