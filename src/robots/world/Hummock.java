package robots.world;

import robots.geometry.*;

import java.util.ArrayList;
import java.util.List;

public class Hummock implements EnvironmentNoise {
    public final Vector position;
    public final Angle angleVariation;

    public Hummock(Vector position, Angle angleVariation) {
        this.position = position;
        this.angleVariation = angleVariation;
    }

    final static double AFFECT_EPS = 1e-6;

    public World affect(World world) {
        if (position.distanceTo(world.robot.position) >= world.robot.config.radius + AFFECT_EPS)
            return world;

        Angle newDirection = new Angle(world.robot.direction.value + angleVariation.value);
        Robot newRobot = new Robot(world.robot.config, world.robot.position, newDirection);

        List<EnvironmentNoise> newNoises = new ArrayList<>();
        for (EnvironmentNoise noise : world.environmentNoises)
            if (noise != this)
                newNoises.add(noise);
        return new World(newRobot, world.robotAI, world.target, newNoises, world.commandInaccuracies);
    }

    public String toString() {
        return String.format("Hummock(%s, %s)", position, angleVariation);
    }
}
