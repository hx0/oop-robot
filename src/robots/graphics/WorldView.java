package robots.graphics;
import robots.geometry.Vector;
import robots.world.*;

import javax.swing.JPanel;
import java.awt.*;

public class WorldView extends JPanel {
    private final static int FRAME_WIDTH = 600;
    private final static int FRAME_HEIGHT = 600;

    private Simulator simulator;

    public WorldView(Simulator simulator) {
        this.simulator = simulator;
        calculateVisibleZone();
    }

    public Dimension getPreferredSize() {
        return new Dimension(FRAME_WIDTH, FRAME_HEIGHT);
    }

    private static Vector min(Vector a, Vector b) {
        return new Vector(Math.min(a.x, b.x), Math.min(a.y, b.y));
    }

    private static Vector max(Vector a, Vector b) {
        return new Vector(Math.max(a.x, b.x), Math.max(a.y, b.y));
    }

    private double scale, offsetX, offsetY;

    private final static double VISIBLE_BUFFER = 100;

    private void calculateVisibleZone() {
        World world = simulator.getWorld();
        double radius = world.robot.config.radius;
        Vector radiusOffsets = new Vector(radius, radius);
        Vector leftTopPoint = min(world.robot.position.subtract(radiusOffsets), world.target);
        Vector rightBottomPoint = max(world.robot.position.add(radiusOffsets), world.target);

        Vector bufferOffsets = new Vector(VISIBLE_BUFFER, VISIBLE_BUFFER);
        leftTopPoint = leftTopPoint.subtract(bufferOffsets);
        rightBottomPoint = rightBottomPoint.add(bufferOffsets);

        Vector center = leftTopPoint.add(rightBottomPoint).multiply(0.5);
        offsetX = center.x;
        offsetY = center.y;
        Vector diagonal = rightBottomPoint.subtract(leftTopPoint);
        scale = Math.max(diagonal.x / FRAME_WIDTH, diagonal.y / FRAME_HEIGHT);
    }

    private void fillCircle(Graphics g, Vector position, double radius) {
        int x = (int)((position.x - radius - offsetX) / scale) + FRAME_WIDTH / 2;
        int y = (int)((position.y - radius - offsetY) / scale) + FRAME_HEIGHT / 2;
        int side = (int)(2 * radius / scale);
        g.fillOval(x, y, side, side);
    }

    private final static double POINT_RADIUS = 4;

    private void drawHummocks(Graphics g) {
        g.setColor(new Color(0x964b00));
        for (EnvironmentNoise noise : simulator.getWorld().environmentNoises) {
            if (!(noise instanceof Hummock))
                continue;
            Hummock hummock = (Hummock) noise;
            fillCircle(g, hummock.position, POINT_RADIUS);
        }
    }

	private void drawRobot(Graphics g) {
        robots.world.Robot robot = simulator.getWorld().robot;
        g.setColor(Color.GRAY);
        fillCircle(g, robot.position, robot.config.radius);
        g.setColor(Color.BLACK);
        fillCircle(g, robot.position, POINT_RADIUS / 2);

		Vector headPosition = robot.getHeadPosition();
		g.setColor(Color.BLUE);
        fillCircle(g, headPosition, POINT_RADIUS);
	}

    private void drawTarget(Graphics g) {
        Vector target = simulator.getWorld().target;
        g.setColor(Color.RED);
        fillCircle(g, target, POINT_RADIUS);
    }

    private final static int TEXT_X = 10;
    private final static int TEXT_Y_INTERVAL = 20;

    private int textY;

    private void printString(Graphics g, String str) {
        g.drawString(str, TEXT_X, textY);
        textY += TEXT_Y_INTERVAL;
    }

    private void printInterval() {
        textY += TEXT_Y_INTERVAL * 0.5;
    }

    private void drawStatus(Graphics g) {
        g.setColor(Color.BLACK);
        textY = 20;

        Font defaultFont = g.getFont();
        g.setFont(defaultFont.deriveFont(Font.BOLD));
        printString(g, simulator.getDescription());
        g.setFont(defaultFont);
        printInterval();

        printString(g, String.format("currentTime = %.1f", simulator.getCurrentTime()));
        printInterval();

        printString(g, String.format("scale = %.1fx", scale));
        printInterval();

        World world = simulator.getWorld();
        printString(g, String.format("robot.position = %s", world.robot.position));
        printString(g, String.format("robot.direction = %s", world.robot.direction));
        printString(g, String.format("robot.getHeadPosition() = %s", world.robot.getHeadPosition()));
        printString(g, String.format("target = %s", world.target));
        //printString(g, String.format("distanceTo(target) = %.1f",
        //        world.robot.getHeadPosition().distanceTo(world.target)));
        printInterval();

        for (EnvironmentNoise noise : world.environmentNoises)
            printString(g, noise.toString());
        for (CommandInaccuracy inaccuracy : world.commandInaccuracies)
            printString(g, inaccuracy.toString());
    }

	@Override
	protected void paintComponent(Graphics g)
	{
        super.paintComponent(g);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, FRAME_WIDTH, FRAME_HEIGHT);

        drawHummocks(g);

		drawRobot(g);
        drawTarget(g);

        drawStatus(g);
	}
}