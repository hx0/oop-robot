package robots.geometry;

public class Vector {
	public final double x, y;

	public Vector(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Vector add(Vector other) {
		return new Vector(x + other.x, y + other.y);
	}

	public Vector subtract(Vector other) {
		return new Vector(x - other.x, y - other.y);
	}

    public Vector multiply(double coeff) {
        return new Vector(x * coeff, y * coeff);
    }

	public double dotProduct(Vector other) {
		return x * other.x + y * other.y;
	}

	public double crossProduct(Vector other) {
		return x * other.y - y * other.x;
	}

	public Vector rotate(Angle angle) {
		double sinAngle = Math.sin(angle.value);
		double cosAngle = Math.cos(angle.value);
		return new Vector(x * cosAngle - y * sinAngle, x * sinAngle + y * cosAngle);
	}

	public double length() {
		return Math.sqrt(x * x + y * y);
	}

	public double distanceTo(Vector other) {
        return subtract(other).length();
    }

    public final static double COMPARE_EPS = 1e-6;

	public boolean equals(Vector other) {
		return Math.abs(x - other.x) < COMPARE_EPS && Math.abs(y - other.y) < COMPARE_EPS;
	}

    public String toString() {
		return String.format("(%.1f, %.1f)", x, y);
	}
}
