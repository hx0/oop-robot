package robots.geometry;

public class Angle {
	public final double value;
	
	public Angle(double value) {
		value = value % (2 * Math.PI);
		if (value < 0)
			value += 2 * Math.PI;
		this.value = value;
	}
	
	public Angle(Vector a, Vector b) {
		double value = Math.atan2(a.crossProduct(b), a.dotProduct(b));
		if (value < 0)
			value += 2 * Math.PI;
		this.value = value;
	}

	public final static double COMPARE_EPS = 1e-6;

	public boolean equals(Angle other) {
		double diff = Math.abs(value - other.value);
		return diff < COMPARE_EPS || 2 * Math.PI - diff < COMPARE_EPS;
	}
	
	public double difference(Angle other) {
		double result = value - other.value;
		if (result > Math.PI)
			result -= 2 * Math.PI;
		else
		if (result < -Math.PI)
			result += 2 * Math.PI;
		return result;
	}
	
	public String toString() {
		return String.format("%.2f * PI rad", value / Math.PI);
	}
}
