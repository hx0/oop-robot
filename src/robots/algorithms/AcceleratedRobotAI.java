package robots.algorithms;

import robots.geometry.*;
import robots.world.*;

public class AcceleratedRobotAI implements RobotAI {
	private final static int TERNARY_SEARCH_ITERS_COUNT = 30;

	private static double calculateFitness(Robot robot, World world,
										   RobotCommand suggestedCommand) {
        Vector summaryWindVelocity = new Vector(0, 0);
        for (EnvironmentNoise noise : world.environmentNoises)
            if (noise instanceof Wind)
                summaryWindVelocity = summaryWindVelocity.add(((Wind) noise).velocity);
        world = new Wind(summaryWindVelocity).affect(world);

        Robot newRobot = world.robot.move(suggestedCommand);
        return newRobot.getHeadPosition().distanceTo(world.target);
	}

	private interface TernarySearchAccessor {
		RobotCommand access(double value);
	}

	private static RobotCommand ternarySearch(Robot robot, World world, double l, double r,
											  TernarySearchAccessor accessor) {
		for (int i = 0; i < TERNARY_SEARCH_ITERS_COUNT; i++) {
			double param1 = l + (r - l) / 3;
			double param2 = l + (r - l) / 3 * 2;
			double fitness1 = calculateFitness(robot, world, accessor.access(param1));
			double fitness2 = calculateFitness(robot, world, accessor.access(param2));

			if (fitness1 >= fitness2)
				l = param1;
			else
				r = param2;
		}
		return accessor.access(l);
	}

	private RobotCommand selectAngleVelocity(final Robot robot, final World world,
											 final double velocity) {
		double realMaxAngleVelocity = Math.min(robot.config.maxAngleVelocity,
				Math.PI / World.TIME_INTERVAL);
		double l = -realMaxAngleVelocity;
		double r = realMaxAngleVelocity;
		return ternarySearch(robot, world, l, r, value -> new RobotCommand(velocity, value));
	}

	private RobotCommand selectVelocity(final Robot robot, final World world) {
		double l = -robot.config.maxVelocity;
		double r = robot.config.maxVelocity;
		return ternarySearch(robot, world, l, r, value -> selectAngleVelocity(robot, world, value));
	}

	public RobotCommand nextMove(Robot robot, World world) {
		return selectVelocity(robot, world);
	}
}
