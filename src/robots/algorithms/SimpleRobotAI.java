package robots.algorithms;

import robots.geometry.*;
import robots.world.Robot;
import robots.world.RobotAI;
import robots.world.RobotCommand;
import robots.world.World;

public class SimpleRobotAI implements RobotAI {
	private static final double EPS = 1e-6;

	public static RobotCommand rotate(Robot robot, World world) {
		Vector centerToTarget = world.target.subtract(robot.position);
		Angle rotationAngle = new Angle(robot.getHeadOffset(), centerToTarget);
        if (rotationAngle.equals(new Angle(0)))
            return null;

        double signedAngle = rotationAngle.value;
        if (signedAngle > Math.PI)
            signedAngle -= 2 * Math.PI;

		double angleVelocity = robot.config.maxAngleVelocity;
        if (signedAngle < 0)
            angleVelocity = -angleVelocity;

		if (signedAngle / angleVelocity < World.TIME_INTERVAL)
			angleVelocity = signedAngle / World.TIME_INTERVAL;

		return new RobotCommand(0, angleVelocity);
	}

	public static RobotCommand linearMove(Robot robot, World world) {
		Vector centerToTarget = world.target.subtract(robot.position); // FIXME: May be 0?
		Vector headToTarget = world.target.subtract(robot.getHeadPosition());
		double distance = headToTarget.length();
		if (distance < EPS)
			return null;

		double velocity = robot.config.maxVelocity;
		if (distance / velocity < World.TIME_INTERVAL)
			velocity = distance / World.TIME_INTERVAL;

		if (centerToTarget.dotProduct(headToTarget) < 0)
			velocity = -velocity;

		return new RobotCommand(velocity, 0);
	}

	public RobotCommand nextMove(Robot robot, World world) {
		RobotCommand command;

		command = rotate(robot, world);
		if (command != null)
			return command;

		command = linearMove(robot, world);
		if (command != null)
			return command;

		return null;
	}
}
