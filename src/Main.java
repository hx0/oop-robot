import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.swing.*;
import java.awt.event.*;

import robots.algorithms.*;
import robots.geometry.*;
import robots.graphics.*;
import robots.world.*;

public class Main {
    private final static double EPS = 1e-6;

	private static World worldFromTestConfig(Hashtable<String, Double> testConfig, RobotAI robotAI) {
		Vector start = new Vector(testConfig.get("robotX"), testConfig.get("robotY"));
		Vector target = new Vector(testConfig.get("targetX"), testConfig.get("targetY"));
		RobotConfig config = new RobotConfig(testConfig.get("radius"),
				testConfig.get("maxLinearSpeed"), testConfig.get("maxAngularSpeed"));

		Robot robot = new Robot(config, start, new Angle(testConfig.get("rotationAngle")));
        List<EnvironmentNoise> environmentNoises = new ArrayList<EnvironmentNoise>() {{
            if (robotAI instanceof AcceleratedRobotAI) {
                double windVelocityAbs = Math.min(robot.config.maxVelocity / 3, 3.0);
                add(new Wind(new Vector(windVelocityAbs, 0).rotate(new Angle(-Math.PI / 4))));
            }
            if (target.equals(new Vector(200, 200))) {
                add(new Hummock(new Vector(140, 140), new Angle(Math.PI / 4)));
                add(new Hummock(new Vector(160, 160), new Angle(Math.PI / 2)));
                add(new Hummock(new Vector(180, 180), new Angle(Math.PI)));
            }
        }};
        List<CommandInaccuracy> commandInaccuracies = new ArrayList<CommandInaccuracy>() {{
            if (robotAI instanceof AcceleratedRobotAI) {
                add(new EngineDefect(robot.config.maxVelocity / 1e2, robot.config.maxAngleVelocity / 1e2));
            }
        }};
		return new World(robot, robotAI, target, environmentNoises, commandInaccuracies);
	}

	private static List<Hashtable<String, Double>> loadCommonTests() {
		/* Common tests from
		 * https://docs.google.com/spreadsheets/d/11sH3EinP55YMkDmcgnneNkb3pC3OFx6YpdjfsemBsLQ/edit
		 */

		List<Hashtable<String, Double>> tests = new ArrayList<>();
		tests.add(new Hashtable<String,Double>(){{put("robotX",100.0);put("robotY",100.0);put("rotationAngle",0.0);put("maxLinearSpeed",10.0);put("maxAngularSpeed",1.0);put("radius",50.0); put("targetX",200.0);put("targetY",200.0);}});
		tests.add(new Hashtable<String,Double>(){{put("robotX",500.0);put("robotY",100.0);put("rotationAngle",Math.PI);put("maxLinearSpeed",1.0);put("maxAngularSpeed",0.05);put("radius",5.0); put("targetX",600.0);put("targetY",200.0);};});
		tests.add(new Hashtable<String,Double>(){{put("robotX",0.0);put("robotY",1.0);put("rotationAngle",2.0);put("maxLinearSpeed",10.0);put("maxAngularSpeed",1.0);put("radius",7.0); put("targetX",20.0);put("targetY",1.0);};});
		tests.add(new Hashtable<String,Double>(){{put("robotX",200.0);put("robotY",700.0);put("rotationAngle",Math.PI / 2);put("maxLinearSpeed",50.0);put("maxAngularSpeed",0.01);put("radius",50.0); put("targetX",600.0);put("targetY",700.0);};});
		tests.add(new Hashtable<String,Double>(){{put("robotX",0.0);put("robotY",0.0);put("rotationAngle",0.0);put("maxLinearSpeed",1.0);put("maxAngularSpeed",1.0);put("radius",1.0); put("targetX",0.0);put("targetY",1000.0);};});
		tests.add(new Hashtable<String,Double>(){{put("robotX",0.0);put("robotY",0.0);put("rotationAngle",0.0);put("maxLinearSpeed",10.0);put("maxAngularSpeed",0.1);put("radius",30.0); put("targetX",-100.0);put("targetY",-200.0);};});
		tests.add(new Hashtable<String,Double>(){{put("robotX",123.0);put("robotY",321.0);put("rotationAngle",Math.PI * 3 / 4);put("maxLinearSpeed",1.0);put("maxAngularSpeed",0.01);put("radius",100.0); put("targetX",120.0);put("targetY",321.0);};});
		tests.add(new Hashtable<String,Double>(){{put("robotX",100.0);put("robotY",200.0);put("rotationAngle",Math.PI / 2);put("maxLinearSpeed",10.0);put("maxAngularSpeed",0.01);put("radius",90.0); put("targetX",100.0);put("targetY",100.0);};});
		return tests;
	}

    private static void sleep(long msces) {
        try {
            Thread.sleep(msces);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public final static int SIMULATION_SPEEDUP = 10;
    public final static int SLOW_SIMULATION_SPEEDUP = 5;
    public final static int SIMULATION_SLEEP_TIME = 10;

    private static double showSimulation(JFrame frame, Simulator simulator) {
        WorldView worldView = new WorldView(simulator);
        frame.add(worldView);
        frame.pack();

        for (int i = 0; ; i++) {
            if (simulator.getWorld().robotAI instanceof AcceleratedRobotAI) {
                if (i % SLOW_SIMULATION_SPEEDUP == 0)
                    worldView.repaint();
            } else {
                if (i % SIMULATION_SPEEDUP == 0) {
                    worldView.repaint();
                    sleep(SIMULATION_SLEEP_TIME);
                }
            }

            if (simulator.makeStep())
                break;
        }

        frame.remove(worldView);
        return simulator.getCurrentTime();
    }

	public static void main(String[] args) {
        JFrame frame = new JFrame("Simulation");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

		List<Hashtable<String, Double>> tests = loadCommonTests();
		for (int i = 0; i < tests.size(); i++) {
			System.out.printf("* Test %d\n", i + 1);
			RobotAI[] robotAIs = new RobotAI[] {new SimpleRobotAI(), new AcceleratedRobotAI()};
			for (RobotAI ai : robotAIs) {
                String description = String.format("Test %d (%s)", i + 1, ai.getClass().getSimpleName());
				World initialWorld = worldFromTestConfig(tests.get(i), ai);
				double minTime = showSimulation(frame, new Simulator(description, initialWorld));

				System.out.printf("%s: %.3f\n", ai.getClass().getSimpleName(), minTime);
			}
			System.out.println();
		}

        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	}
}
